package a8.view;

public abstract class ChessClickEvent extends ChessViewEvent {
	public boolean isSecondClick(){
		return false;
	}
	public abstract int getX();
	public abstract int getY();

}
