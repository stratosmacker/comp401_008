package a8.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
@SuppressWarnings("serial")
public class BoardSpot extends JButton implements MouseListener{
	
	JPositionView position;
	
	Color orgColor;
	private boolean colorLock;
	
	public BoardSpot(JPositionView p, Color c){
		this.setFont(new Font("sans", Font.BOLD, 35));
		position = p;
		orgColor = c;
		this.setBackground(c);
		this.addMouseListener(this);
		colorLock = false;
	}
	public JPositionView getPosition(){
		return position;
	}
	
	//the mouseListener impl is not for moving pieces, that uses the newer ActionListener implemented in ChessGameView
	//this is for cooler stuf
	@Override
	public void mouseClicked(MouseEvent arg0) {}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		if(arg0.getSource() == this){
			this.setBackground(Color.blue);
		}
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		if(arg0.getSource() == this && !colorLock){
			this.setToOrgColor();
		}
	}
	public void setToOrgColor(){
		this.setBackground(orgColor);
	}
	@Override
	public void mousePressed(MouseEvent arg0) {}
	@Override
	public void mouseReleased(MouseEvent arg0) {}
	public void armColorLock() {
		colorLock = true;
	}
	public void disarmColorLock() {
		colorLock = false;
	}
	
	
}
