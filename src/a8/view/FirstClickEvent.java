package a8.view;

public class FirstClickEvent extends ChessClickEvent{
	private int x, y;
	
	public FirstClickEvent(int x, int y){
		this.x = x; this.y = y;
	}
	@Override
	public int getX() {
		return x;
	}
	@Override
	public int getY() {
		return y;
	}
}