package a8.view;

//this class would have been related to the real deal, but it much more work to make inheritance work when the other class was already built

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ChessGameViewRepresentation extends JPanel{

	BoardSpot[][] boardSpot;
	JLabel p2Label;
	JLabel p1Label;
	GridBagLayout gbd;
	String p1, p2;

	public ChessGameViewRepresentation(String p1, String p2){
		this.p1 = p1; this.p2 = p2;
		gbd = new GridBagLayout();
		boardSpot = new BoardSpot[8][8];
		//set this layout
		this.setLayout(gbd);
		//constraints for later
		GridBagConstraints c = new GridBagConstraints();

		//p2 Label
		GridBagConstraints c2 = new GridBagConstraints();
		p2Label = new JLabel(p2);
		p2Label.setOpaque(true);
		p2Label.setHorizontalAlignment(JLabel.CENTER);
		p2Label.setFont(new Font("SansSerif", Font.ITALIC, 15));
		//Constraints p2
		c2.fill = GridBagConstraints.BOTH;
		c2.ipadx = 1;
		c2.ipady = 1;
		c2.gridx = 0;
		c2.gridy = 0;
		c2.gridwidth = 8;
		c2.gridheight = 1;
		c2.weightx = 0.5;
		c2.weighty = 0.5;
		this.add(p2Label, c2);

		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				//SOME OF THIS CODE IS FROM RECITATION
				//Used when the component's display area is larger
				//than the component's requested size to determine
				//whether and how to resize the component.
				c.fill = GridBagConstraints.BOTH;
				//Internal X and Y padding
				c.ipadx = 10;
				c.ipady = 10;
				//Grid position (i, j)
				c.gridx = i;
				//NOTE THAT THE Y VALUES HERE ARE INVERTED + 1
				c.gridy = 8 - j + 1;
				//Weights (0.0 to 1.0) are used to determine how to distribute 
				//space among columns (weightx) and among rows (weighty);
				//this is important for specifying resizing behavior.
				c.weightx = 0.5;
				c.weighty = 0.5;

				//bkgrd clr with instansiation
				if((i+j) % 2 == 0){
					boardSpot[i][j] = new BoardSpot(new JPositionView(i,j), Color.WHITE);
				}
				else{
					boardSpot[i][j] = new BoardSpot(new JPositionView(i,j), Color.GRAY);
				}
				//Add button to panel
				this.add(boardSpot[i][j], c);
			}
		}

		//p1 Label
		p1Label = new JLabel(p1);
		p1Label.setOpaque(true);
		p1Label.setHorizontalAlignment(JLabel.CENTER);
		p1Label.setFont(new Font("SansSerif", Font.ITALIC, 15));
		//Constraints
		c2.gridy = 10;
		this.add(p1Label, c2);
	}
	public void setBoardSpots(char[][] c){
		for(int i = 0; i < 8; i++){
			for(int j = 0; j<8; j++){
				this.boardSpot[i][j].setText(ChessGameView.getCoolChessChar(c[i][j]) + "");
			}
		}
	}
	public static char getCoolChessChar(char mark){
		//maybe later
		//need to have special font
		char c = mark;
		switch(mark){
			case('R'):
				//whiterook
				c = '\u2656';
				break;
			case('r'):
				//blackrook
				c = '\u265C';break;
			case('K'):
				//whiteKing
				c = '\u2654';break;
			case('k'):
				//blackking
				c = '\u265A';break;
			case('P'):
				//whitepawn
				c = '\u2659';break;
			case('p'):
				//blackpawn
				c = '\u265F';break;
			case('Q'):
				//whiteQueen
				c = '\u2655';break;
			case('q'):
				//blackqueen
				c = '\u265B';break;
			case('B'):
				//whitebishop
				c = '\u2657';break;
			case('b'):
				//blackbishop
				c = '\u265D';break;
			case('N'):
				//whiteknight
				c = '\u2658';break;
			case('n'):
				//blackknight
				c = '\u265E';break;
		}
		
		return c;
		
	}

}
