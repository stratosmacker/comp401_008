package a8.view;

public class SecondClickEvent extends ChessClickEvent{
	private int x, y;
	
	public SecondClickEvent(int x, int y){
		this.x = x; this.y = y;
	}
	@Override
	public boolean isSecondClick(){
		return true;
	}
	@Override
	public int getX() {
		return x;
	}
	@Override
	public int getY() {
		return y;
	}
}