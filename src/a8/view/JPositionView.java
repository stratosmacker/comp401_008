package a8.view;

//refers to x, y on the viewable JFrame board with 0,0 being in the bottom left
public class JPositionView {
	
	private int x;
	private int y;
	
	public JPositionView(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
	
	@Override
	public boolean equals(Object o) {
		JPositionView p = (JPositionView) o;
		return ((p.getX() == x) && (p.getY() == y));
	}
}