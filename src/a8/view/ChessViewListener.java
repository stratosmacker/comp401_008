package a8.view;

public interface ChessViewListener {

	void handleChessViewEvent(ChessViewEvent e);
}
