package a8.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

@SuppressWarnings("serial")
public class ChessGameView extends ChessGameViewRepresentation implements ActionListener{
	
	boolean secondClick;
	JPositionView onePos;
	private List<ChessViewListener> listeners;
	private StyledDocument logView;
	private StyledDocument deadGuys;
	private Stack<String> oldLogView;
	private Stack<Boolean> hasJustSentToGraveYard;
	JTextPane deadGuyText;
	public ChessGameView(String p1, String p2) {
		super(p1,p2);
		//instansiate var
		secondClick = false;
		listeners = new ArrayList<ChessViewListener>();
		oldLogView = new Stack<String>();
		hasJustSentToGraveYard = new Stack<Boolean>();
		//note that these start at null (for now?)
		onePos = null;
        //addactionListenersto boardspot
        for(BoardSpot b[] : boardSpot){
        	for(BoardSpot bb: b){
        		bb.addActionListener(this);
        	}
        }
        //side panel
        JPanel extra = new JPanel();
        extra.setLayout(new BoxLayout(extra, BoxLayout.X_AXIS));
        //the visual Log
        JTextPane visualLogText = new JTextPane();
        JScrollPane visualLog = new JScrollPane(visualLogText);
        visualLog.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        visualLog.setEnabled(false);
        visualLogText.setFont(new Font("SansSerif", Font.ITALIC, 10));
        visualLogText.setEditable(false);
        visualLogText.setBackground(Color.BLACK);
        extra.add(visualLog);
        //for Adding text to the TextPane
        logView = visualLogText.getStyledDocument();
        
        //set perm size
        visualLog.setPreferredSize(new Dimension(300, 0));
        
        //Undo button
        JButton undoButton = new UndoButton("Undo");
        undoButton.addActionListener(this);
        undoButton.setAlignmentX(LEFT_ALIGNMENT);
        extra.add(undoButton);
        
        //graveyard representation
        deadGuyText = new JTextPane();
        JScrollPane deadGuy = new JScrollPane(deadGuyText);
        deadGuy.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        deadGuy.setEnabled(false);
        deadGuyText.setFont(new Font("SansSerif", Font.ITALIC, 20));
        deadGuyText.setEditable(false);
        deadGuyText.setBackground(Color.WHITE);
        extra.add(deadGuy);
        deadGuys = deadGuyText.getStyledDocument();
        deadGuy.setPreferredSize(new Dimension(200, 300));
        
        //Constraints c3
        GridBagConstraints c3 = new GridBagConstraints();
        c3.fill = GridBagConstraints.BOTH;
        c3.ipadx = 1;
        c3.ipady = 1;
        c3.gridx = 11;
        c3.gridy = 0;
        c3.gridwidth = 5;
        c3.gridheight = 6;
        c3.weightx = 0.5;
        c3.weighty = 0.5;
        this.add(extra, c3);
	}
	public void appendTextToLogView(String string) {
		//unless it says undo
		if(string.equals("undo")){
			appendTextToLogView(oldLogView.pop() + " has been undone");
			oldLogView.pop();
		}
		else{
	        //  Define a keyword attribute
	        SimpleAttributeSet keyWord = new SimpleAttributeSet();
	        StyleConstants.setForeground(keyWord, Color.GREEN);
	        StyleConstants.setBackground(keyWord, Color.BLACK);
	    	try {
				logView.insertString(logView.getLength(), string + "\n", keyWord );
				oldLogView.push(string);
			} catch (BadLocationException e) {
				// caught here because I dont want the controller handling view specific exceptions
				e.printStackTrace();
			}
		}
	}
	public void appendDeadGuys(Character character) {
        //  Define a keyword attribute
        SimpleAttributeSet keyWord = new SimpleAttributeSet();
        StyleConstants.setBackground(keyWord, Color.WHITE);
        StyleConstants.setForeground(keyWord, Color.BLACK);
		//unless it says undo
		if(character.equals('u') && this.hasJustSentToGraveYard.peek()){
			try {
				deadGuys.insertString(deadGuys.getLength(), "<<<undone \n", keyWord );
				this.hasJustSentToGraveYard.pop();
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}
		else if(!character.equals('u')){
	    	try {
				deadGuys.insertString(deadGuys.getLength(), ChessGameView.getCoolChessChar(character) + "", keyWord );
			} catch (BadLocationException e) {
				// caught here because I dont want the controller handling view specific exceptions
				e.printStackTrace();
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() instanceof BoardSpot){
			//for the buttons
			BoardSpot b = (BoardSpot) e.getSource();
			if(secondClick){
				//second click
				dispatchChessEvent(b.getPosition(), secondClick, EventType.BOARD_CLICK);
				secondClick = false;
				resetColors();
			}
			else{
				//firstclick
				onePos = b.getPosition();
				dispatchChessEvent(b.getPosition(), secondClick, EventType.BOARD_CLICK);
				secondClick = true;
				b.setBackground(Color.red);
				b.armColorLock();
			}
		}
		else if(e.getSource() instanceof UndoButton){
			dispatchChessEvent(null, false, EventType.UNDO);
		}

	}
	private enum EventType{
		BOARD_CLICK, UNDO
	}
	private void dispatchChessEvent(JPositionView position, boolean isSecondClick, EventType t) {
		if (t == EventType.BOARD_CLICK) {
			if (isSecondClick) {
				fireEvent(new SecondClickEvent(position.getX(), position.getY()));
			} else {
				fireEvent(new FirstClickEvent(position.getX(), position.getY()));
			}
		}
		else if(t == EventType.UNDO){
			fireEvent(new UndoEvent());
		}
		
	}
	protected void resetColors(){
		for(BoardSpot[] bd: boardSpot){
			for(BoardSpot b: bd){
				b.disarmColorLock();
				b.setToOrgColor();
			}
		}
	}
	public void addChessViewListener(ChessViewListener l) {
		listeners.add(l);
	}
	public void removeChessViewListener(ChessViewListener l) {
		listeners.remove(l);
	}
	public void fireEvent(ChessViewEvent e) {
		for (ChessViewListener l : listeners) {
			l.handleChessViewEvent(e);
		}
	}
	public void setTurnIndicator(String name) {
		if(name.equals(p1)){
			p1Label.setBackground(Color.RED);
			p2Label.setBackground(Color.WHITE);
		}
		else{
			p2Label.setBackground(Color.RED);
			p1Label.setBackground(Color.WHITE);
		}
	}
	public void appendDeadGuys(String s) {
		this.appendDeadGuys(s.charAt(0));
		
	}
	public void addJustSentToGraveYard(boolean b){
		this.hasJustSentToGraveYard.push(b);
	}
	
}



