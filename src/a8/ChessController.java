package a8;

import java.util.Observable;
import java.util.Observer;
import a8.model.ChessGameModel;
import a8.view.ChessClickEvent;
import a8.view.ChessGameView;
import a8.view.ChessViewEvent;
import a8.view.ChessViewListener;
import a8.view.UndoEvent;

public class ChessController implements Observer, ChessViewListener{
	private ChessGameModel game;
	private ChessGameView view;
	private int firstX, firstY;
	
	
	private ChessController(ChessGameModel game, ChessGameView view){
		this.view = view;
		this.game = game;
		view.addChessViewListener(this);
		
		//-1 means not initialized
		firstX = -1; firstY = -1;
		
		game.addObserver(this);
		
		//show the board
		this.update(game, null);
		view.setTurnIndicator(this.getPlayer1());
	}
	public static ChessController makeController(ChessGameModel game, ChessGameView view){
		return new ChessController(game, view);
	}
	@Override
	public void update(Observable arg0, Object arg1) {
		//The model hasChanged so update the view
		char[][] boardChars = this.getCharacterBoard();
		view.setBoardSpots(boardChars);
		if(arg1 instanceof String){
			String s = (String) arg1;
			view.appendTextToLogView(s);
			if(s.equals("undo"))
				view.appendDeadGuys(s);
			//Set the turn indicator
			view.setTurnIndicator(this.getTurnName());
		}
		if(hasGraveYardChanged()){
			view.appendDeadGuys(game.getGraveYard().peek());
			view.addJustSentToGraveYard(true);
		}
		else{
			view.addJustSentToGraveYard(false);
		}

	}
	@Override
	public void handleChessViewEvent(ChessViewEvent e) {
		//the view has changed so update the model
		if(e instanceof ChessClickEvent){
			ChessClickEvent ev = (ChessClickEvent) e;
			if(ev.isSecondClick()){
				game.move(firstX, firstY, ev.getX(), ev.getY());
				this.firstX = -1; this.firstY = -1;
			}
			else{
				firstX = ev.getX(); firstY = ev.getY();
			}
		}
		else if(e instanceof UndoEvent){
			undo();
		}
		
	}
	private char[][] getCharacterBoard(){
		//returns an array of characters that is the board
		return game.getCharacterBoard();
	}
	private String getTurnName(){
		return game.getTurnName();	
	}
	private String getPlayer1(){
		return game.getPlayer1().getName();
	}
	@SuppressWarnings("unused")
	private String getPlayer2(){
		return game.getPlayer2().getName();
	}
	private void undo(){
		game.undo();
	}
	private boolean hasGraveYardChanged(){
		return game.hasGraveYardChanged();
	}
}
