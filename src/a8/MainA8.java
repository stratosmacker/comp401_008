package a8;

import javax.swing.JFrame;

import a8.model.ChessGameModel;
import a8.view.ChessGameView;
public class MainA8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame main_frame = new JFrame();
		main_frame.setTitle("Chess Game");
		main_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		String name1 = "Player 1";
		String name2 = "Player 2";
		ChessGameView game_view = new ChessGameView(name1, name2);
		ChessGameModel game_model = new ChessGameModel(name1, name2);
		
		main_frame.setContentPane(game_view);

		@SuppressWarnings("unused")
		ChessController controler = ChessController.makeController(game_model, game_view);
		
		main_frame.pack();
		main_frame.setVisible(true);
	}

}
