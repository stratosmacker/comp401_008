package a8.model;

import java.util.Observable;
import java.util.Stack;

public class ChessGameModel extends Observable{

	private ChessBoardModel board;
	private Stack<Character> deadBoard;
	private ChessPlayerModel player1;
	private ChessPlayerModel player2;
	private enum Turn{
		P1, P2;
	}
	private Turn turn;
	private Stack<ChessMoveModel> log;
	private boolean graveyardChanged;
	
	public ChessGameModel(String p1, String p2) {
		this.graveyardChanged = false;
		this.player1 = new ChessPlayerModel(p1);
		this.player2 = new ChessPlayerModel(p2);
		board = new ChessBoardModel();
		deadBoard = new Stack <Character>();
		log = new Stack<ChessMoveModel>();
		setTurn(Turn.P1);
		
		new Rook(player1, this, new ChessPositionModel(0,0));
		new Knight(player1, this, new ChessPositionModel(1,0));
		new Bishop(player1, this, new ChessPositionModel(2,0));
		new Queen(player1, this, new ChessPositionModel(3,0));
		new King(player1, this, new ChessPositionModel(4,0));
		new Bishop(player1, this, new ChessPositionModel(5,0));
		new Knight(player1, this, new ChessPositionModel(6,0));
		new Rook(player1, this, new ChessPositionModel(7,0));

		for (int i=0; i<8; i++) {
			new Pawn(player1, this, new ChessPositionModel(i,1));
		}

		new Rook(player2, this, new ChessPositionModel(0,7));
		new Knight(player2, this, new ChessPositionModel(1,7));
		new Bishop(player2, this, new ChessPositionModel(2,7));
		new Queen(player2, this, new ChessPositionModel(3,7));
		new King(player2, this, new ChessPositionModel(4,7));
		new Bishop(player2, this, new ChessPositionModel(5,7));
		new Knight(player2, this, new ChessPositionModel(6,7));
		new Rook(player2, this, new ChessPositionModel(7,7));

		for (int i=0; i<8; i++) {
			new Pawn(player2, this, new ChessPositionModel(i,6));
		}		
	}
	private boolean isCorrectPlayer(ChessPlayerModel p){
		if(p.equals(this.player1)){
			return turn == Turn.P1;
		}
		else{
			return turn == Turn.P2;
		}
	}
	private void nextPlayerTurn(){
		if(turn.equals(Turn.P1)){
			turn = Turn.P2;
		}
		else{
			turn = Turn.P1;
		}
	}
	
	public ChessPlayerModel getPlayer1() {
		return player1;
	}
	
	public ChessPlayerModel getPlayer2() {
		return player2;
	}

	public ChessBoardModel getBoard() {
		return board;
	}

	public void move(int firstX, int firstY, int sX, int sY) {
		
		//if you can move, and then notify the observers so that everything is updated
		
		ChessPositionModel from = new ChessPositionModel(firstX, firstY);
		ChessPositionModel to = new ChessPositionModel(sX, sY);
		ChessPieceModel piece = board.getPieceAt(from);
		ChessPieceModel captured = board.getPieceAt(to);
		
		ChessMoveModel m = new ChessMoveModel(piece, from, to, captured);
		try {
			if (piece != null) {
				if (isCorrectPlayer(piece.getOwner())) {
					piece.moveTo(m.getTo());
					if(captured != null)
						sendToGraveYard(m.getCaptured());
					log.push(m);
					this.setChanged();
					//only pass new move
					this.notifyObservers(m.toString());
					nextPlayerTurn();
				} else {
					throw new WrongTurn(piece.getOwner().getName());
				}
			}
		} catch (IllegalMove | WrongTurn e) {
			System.out.println(e.toString());
		}
	}

	private void sendToGraveYard(ChessPieceModel captured) {
		this.deadBoard.push(captured.getMark());
		this.setGraveYardChanged();
	}
	private void setGraveYardChanged() {
		graveyardChanged = true;
	}
	public boolean hasGraveYardChanged(){
		return this.graveyardChanged;
	}
	private void undoFromGraveYard(){
		this.deadBoard.pop();
	}
	public Stack<Character> getGraveYard(){
		this.graveyardChanged = false;
		return this.deadBoard;
	}
	public char[][] getCharacterBoard(){
		char [][] t = new char[8][8];
		for(int i = 0; i< 8; i ++){
			for(int j = 0; j < 8; j++){
				if(this.getBoard().getPieceAt(new ChessPositionModel(i,j)) != null)
					t [i][j] = this.getBoard().getPieceAt(new ChessPositionModel(i,j)).getMark();
				else
					t[i][j] = ' ';
			}
		}
		return t;
	}

	public Turn getTurn() {
		return turn;
	}

	public void setTurn(Turn turn) {
		this.turn = turn;
	}
	public String getTurnName(){
		//This may seem backwards but it is not
		if(this.getTurn() == Turn.P1){
			return this.getPlayer2().getName();
		}
		else{
			return this.getPlayer1().getName();
		}
	}
	public void undo(){
		if(log.size() > 0){
			//get and remove from log
			ChessMoveModel lMove = log.pop();
			//reset the mover
			getBoard().removePieceFrom(lMove.getTo());
			getBoard().placePieceAt(lMove.getPiece(), lMove.getFrom());
			resetPawnFirstMove(lMove.getPiece());
			//reset the destroyed objecct
			if(lMove.getCaptured() != null){
				getBoard().placePieceAt(lMove.getCaptured(), lMove.getTo());
				resetPawnFirstMove(lMove.getCaptured());
				this.undoFromGraveYard();
			}
		}
		this.setChanged();
		this.notifyObservers("undo");
		this.nextPlayerTurn();
	}
	private void resetPawnFirstMove(ChessPieceModel piece) {
		if(piece instanceof Pawn){
			Pawn pawn = (Pawn) piece;
			//reset the pawn double move
			//admittedly this is a little messy, but I cant let it glitch
			//baseY is the Y value on the board that the respective pawn had to be @ in order to qualify for first move reset
			int baseY;
			if(pawn.getOwner().equals(getPlayer1())){
				//if the pawn in player1's
				baseY = 1;
			}
			else{
				baseY = 6;
			}
			
			//if it had been in it's first spot, reset the firstMove
			if(pawn.getPosition().getY() == baseY){
				pawn.resetFirstMove();
			}
		}
		
	}
	
}