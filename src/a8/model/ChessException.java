package a8.model;

@SuppressWarnings("serial")
public abstract class ChessException extends Exception {
	protected ChessException(String message) {
		super(message);
	}
}

@SuppressWarnings("serial")
class IllegalMove extends ChessException {
	
	public IllegalMove(ChessPieceModel p, ChessPositionModel from, ChessPositionModel to) {
		super("Illegal move: piece " + p.toString() + " can not move from " + from.toString() + " to " + to.toString());
	}
}