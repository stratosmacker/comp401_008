package a8.model;

public class ChessPlayerModel {

	private String name;
	
	public ChessPlayerModel(String name) {
		this.name = name;
	}	
	
	public String getName() {
		return name;
	}
}