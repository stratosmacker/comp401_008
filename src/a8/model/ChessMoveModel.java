package a8.model;

public class ChessMoveModel {

	private ChessPieceModel piece;
	private ChessPositionModel from;
	private ChessPositionModel to;
	private ChessPieceModel captured;
	
	public ChessMoveModel(ChessPieceModel piece, ChessPositionModel from, ChessPositionModel to, ChessPieceModel captured) {
		this.piece = piece;
		this.from = from;
		this.to = to;
		this.captured = captured;
	}

	public ChessPieceModel getPiece() {
		return piece;
	}
	
	public ChessPositionModel getFrom() {
		return from;
	}

	public ChessPositionModel getTo() {
		return to;
	}
	
	public ChessPieceModel getCaptured() {
		return captured;
	}
	
	public boolean pieceWasCaptured() {
		return (captured != null);
	}
	
	@Override
	public String toString() {
		String result =  piece.getOwner().getName() + "'s " + piece.toString() + " moved from " + from.toString() + " to " + to.toString();
		
		if (pieceWasCaptured()) {
			result += " capturing " + captured.getOwner().getName() + "'s " + captured.toString();
		}
		return result;
	}
}
