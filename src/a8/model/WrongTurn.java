package a8.model;

@SuppressWarnings("serial")
public class WrongTurn extends ChessException {

	public WrongTurn(String player) {
		super("It is not " + player + "'s turn" );
	}

}
