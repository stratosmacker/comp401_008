package a8.model;
//Jesse Osiecki

public abstract class ChessPieceModel{

	private ChessPlayerModel owner;
	private ChessGameModel game;
	private ChessPositionModel position;
	private ChessPositionModel initPosition;
	protected char mark;
	
	protected ChessPieceModel(ChessPlayerModel owner, ChessGameModel game, ChessPositionModel init_position) {
		this.owner = owner;
		this.game = game;
		this.position = null;
		this.initPosition = init_position;
		
		game.getBoard().placePieceAt(this, init_position);
		
	}
	public ChessPositionModel getInitPosition(){
		return this.initPosition;
	}
	
	public ChessPlayerModel getOwner() {
		return owner;
	}
	
	public ChessGameModel getGame() {
		return game;
	}
	
		
	public ChessPositionModel getPosition() {
		return position;
	}
	
	public void setPosition(ChessPositionModel new_position) {
		position = new_position;
	}
	
	
	public void moveTo(ChessPositionModel destination) 
		throws IllegalMove
	{
		// Replace with your code as necessary
		throw new IllegalMove(this, position, destination);
	}
	
	public char getMark() {
		return mark;
	}
	
	protected void moveToRec(ChessPositionModel destination, ChessPositionModel orig) throws IllegalMove{
		//this is a helper method for pieces that require a line of sight
		//this repeats incrementing each move, until it is successful or can no longer move.
		
		//unit vector
		ChessPositionModel oneAhead = new ChessPositionModel(unitVect(destination.getX() - this.getPosition().getX()) + this.getPosition().getX(), 
				unitVect(destination.getY() - this.getPosition().getY()) + this.getPosition().getY());
		ChessPieceModel oneAheadPiece = getGame().getBoard().getPieceAt(oneAhead);
		
		if(oneAheadPiece == null){
			//if null no problems
			//move
			this.getGame().getBoard().removePieceFrom(oneAhead);
			this.getGame().getBoard().removePieceFrom(getPosition());
			this.getGame().getBoard().placePieceAt(this, oneAhead);
			if(!destination.equals(oneAhead)){
				//not done, do again
				moveToRec(destination, orig);
			}
		}
		else if(!oneAheadPiece.getOwner().equals(this.getOwner())){
			//the piece ahead is an enemy
			if(oneAhead.equals(destination)){
				//if this is the intended destination, this is ok, remove enemy and finish
				this.getGame().getBoard().removePieceFrom(oneAhead);
				this.getGame().getBoard().removePieceFrom(getPosition());
				this.getGame().getBoard().placePieceAt(this, oneAhead);
			}
			else{
				//cannot move through an enemy piece
				//THESE TWO LINES ADDED SINCE FIVE, THIS WAS MY PROBLEM
				this.getGame().getBoard().removePieceFrom(getPosition());
				this.getGame().getBoard().placePieceAt(this, orig);
				throw new IllegalMove(this, orig, destination);
			}
		}
		else{
			//cant move through own piece, reset and throw error
			this.getGame().getBoard().removePieceFrom(getPosition());
			this.getGame().getBoard().placePieceAt(this, orig);
			throw new IllegalMove(this, orig, destination);
		}
	}
	protected int unitVect(int in){
		if(in > 0){
			return 1;
		}else if(in <0){
			return -1;
			}
		else{
			return 0;
		}
	}
}

class Rook extends ChessPieceModel {
	public Rook(ChessPlayerModel owner, ChessGameModel game, ChessPositionModel init_position) {
		super(owner, game, init_position);
		if (owner == game.getPlayer1()) {
			mark = 'r';
		} else {
			mark = 'R';
		}
	}
	
	public void moveTo(ChessPositionModel destination) throws IllegalMove{
		int dY = this.getPosition().getY() - destination.getY();
		int dX = this.getPosition().getX() - destination.getX();
		ChessPieceModel destinationPiece  = getGame().getBoard().getPieceAt(destination);
		//Start conditionals
		if(destinationPiece != null){
			//same owner, initial square
			if(destinationPiece.getOwner().equals(this.getOwner()))
				throw new IllegalMove(this, this.getPosition(), destination);
			else if((dX == 0 && dY != 0) || (dX != 0 && dY == 0)){
				moveToRec(destination, this.getPosition());
			}
			else{
				throw new IllegalMove(this, this.getPosition(), destination);
			}
		}
		else if((dX == 0 && dY != 0) || (dX != 0 && dY == 0)){
			moveToRec(destination, this.getPosition());
		}
		else{
			throw new IllegalMove(this, this.getPosition(), destination);
		}
	}
	
	public String toString(){
		return "rook";
	}
}

class Bishop extends ChessPieceModel {
	public Bishop(ChessPlayerModel owner, ChessGameModel game, ChessPositionModel init_position) {
		super(owner, game, init_position);
		if (owner == game.getPlayer1()) {
			mark = 'b';
		} else {
			mark = 'B';
		}
	}
	public void moveTo(ChessPositionModel destination) throws IllegalMove{
		int dY = this.getPosition().getY() - destination.getY();
		int dX = this.getPosition().getX() - destination.getX();
		ChessPieceModel destinationPiece = getGame().getBoard().getPieceAt(destination);
		//Start conditionals
		if(Math.abs(dX) < 1 || Math.abs(dY) < 1){
			//diagonal
			throw new IllegalMove(this, this.getPosition(), destination);
		}
		else if(destinationPiece != null){
			//same owner
			if(destinationPiece.getOwner() == this.getOwner())
				throw new IllegalMove(this, this.getPosition(), destination);
			else{
				moveToRec(destination, this.getPosition());
			}
		}
		else{
			moveToRec(destination, this.getPosition());
		}
	}
	public String toString(){
		return "bishop";
	}
}


class Knight extends ChessPieceModel {
	public Knight(ChessPlayerModel owner, ChessGameModel game, ChessPositionModel init_position) {
		super(owner, game, init_position);
		if (owner == game.getPlayer1()) {
			mark = 'n';
		} else {
			mark = 'N';
		}
	}
	
	public void moveTo(ChessPositionModel destination) throws IllegalMove{
		int dY = this.getPosition().getY() - destination.getY();
		int dX = this.getPosition().getX() - destination.getX();
		ChessPieceModel destinationPiece = getGame().getBoard().getPieceAt(destination);
		//Start conditionals
		if(!((Math.abs(dY) == 2 && Math.abs(dX) == 1) ^ (Math.abs(dY) == 1 && Math.abs(dX) == 2))){
			//trying to move too far, or in an unintended way, sorry buddy
			throw new IllegalMove(this, this.getPosition(), destination);
		}
		else if(destinationPiece != null){
			//same owner
			if(destinationPiece.getOwner().getName().equals(this.getOwner().getName()))
				throw new IllegalMove(this, this.getPosition(), destination);
			else{
				//No problemsss
				this.getGame().getBoard().removePieceFrom(destination);
				this.getGame().getBoard().removePieceFrom(getPosition());
				this.getGame().getBoard().placePieceAt(this, destination);
			}
		}
		else{
			//No problemsss
			this.getGame().getBoard().removePieceFrom(destination);
			this.getGame().getBoard().removePieceFrom(getPosition());
			this.getGame().getBoard().placePieceAt(this, destination);
		}
	}
	public String toString(){
		return "knight";
	}
}

class Queen extends ChessPieceModel {
	public Queen(ChessPlayerModel owner, ChessGameModel game, ChessPositionModel init_position) {
		super(owner, game, init_position);
		if (owner == game.getPlayer1()) {
			mark = 'q';
		} else {
			mark = 'Q';
		}
	}	
	public void moveTo(ChessPositionModel destination) throws IllegalMove{
		ChessPieceModel destinationPiece = getGame().getBoard().getPieceAt(destination);
		//Start conditionals
		if(destinationPiece == null){
			moveToRec(destination, this.getPosition());
		}
		else if(!destinationPiece.getOwner().equals(getOwner())){
			moveToRec(destination, this.getPosition());
		}
		else{
			throw new IllegalMove(this, getPosition(), destination);
		}
	}
	public String toString(){
		return "queen";
	}
}

class King extends ChessPieceModel{
	public King(ChessPlayerModel owner, ChessGameModel game, ChessPositionModel init_position) {
		super(owner, game, init_position);
		if (owner == game.getPlayer1()) {
			mark = 'k';
		} else {
			mark = 'K';
		}
	}
	public void moveTo(ChessPositionModel destination) throws IllegalMove{
		ChessPieceModel destinationPiece = getGame().getBoard().getPieceAt(destination);
		int dY =  destination.getY() - this.getPosition().getY();
		int dX =  destination.getX() - this.getPosition().getX();
		
		//Start conditionals
		if(Math.abs(dY) > 1 || Math.abs(dX) > 1){
			//more than one space
			throw new IllegalMove(this, this.getPosition(), destination);
		}
		else if(destinationPiece != null){
			if(destinationPiece.getOwner().getName().equals(this.getOwner().getName())){
				//same owner
				throw new IllegalMove(this, this.getPosition(), destination);
			}
			else{
				this.getGame().getBoard().removePieceFrom(destination);
				this.getGame().getBoard().removePieceFrom(getPosition());
				this.getGame().getBoard().placePieceAt(this, destination);
			}
		}
		else{
			this.getGame().getBoard().removePieceFrom(destination);
			this.getGame().getBoard().removePieceFrom(getPosition());
			this.getGame().getBoard().placePieceAt(this, destination);
		}
	}
	public String toString(){
		return "king";
	}
}

class Pawn extends ChessPieceModel {
	boolean firstMove;
	public Pawn(ChessPlayerModel owner, ChessGameModel game, ChessPositionModel init_position) {
		super(owner, game, init_position);
		if (owner == game.getPlayer1()) {
			mark = 'p';
		} else {
			mark = 'P';
		}
		firstMove = true;
	}
	public void moveTo(ChessPositionModel destination) throws IllegalMove{
		int dY, dX;
		ChessPieceModel destinationPiece = this.getGame().getBoard().getPieceAt(destination);
		if(this.getOwner().equals(this.getGame().getPlayer1())){
			//This could be cleaner
			//player 1 dx/y (positive)
			dY =  destination.getY() - this.getPosition().getY();
			dX =  destination.getX() - this.getPosition().getX();
		}
		else{
			//player 2 dx/y (positive)
			dY = this.getPosition().getY() - destination.getY();
			dX = this.getPosition().getX() - destination.getX();
		}


		//start conditionals
		if(destination.equals(getPosition())){
			throw new IllegalMove(this, getPosition(), destination);
		}
		else if(dY < 0){
			//cannot move backwards
			throw new IllegalMove(this, this.getPosition(), destination);
		}
		else if(Math.abs(dX) == 1 && Math.abs(dY) == 1){
			//diagonal one
			if(destinationPiece != null && !destinationPiece.getOwner().equals(this.getOwner())){
				//check for neither self nor null (other player)
				this.getGame().getBoard().removePieceFrom(destination);
				this.getGame().getBoard().removePieceFrom(getPosition());
				this.getGame().getBoard().placePieceAt(this, destination);
				//success
			}
			else{
				//not somebodyelse
				throw new IllegalMove(this, this.getPosition(), destination);
			}
		}
		else if(dY > 1 && dY < 3){
			//more than one move check
			if(firstMove){
				moveToRec(destination, this.getPosition());
			}
			else{
				throw new IllegalMove(this, this.getPosition(), destination);
			}
		}
		else if(dY == 1){
			//peachy
			moveToRec(destination, this.getPosition());
		}
		else{
			throw new IllegalMove(this, this.getPosition(), destination);
		}
		
		
		//firstmove
		if(this.getPosition().equals(destination)){
			firstMove = false;
		}
	}
	protected void moveToRec(ChessPositionModel destination, ChessPositionModel orig) throws IllegalMove{
		//this is a helper method for pieces that require a line of sight
		//this repeats incrementing each move, until it is successful or can no longer move.
		//modified for the pawn
		
		//unit vector
		ChessPositionModel oneAhead;
		oneAhead = new ChessPositionModel(unitVect(destination.getX() - this.getPosition().getX()) + this.getPosition().getX(), 
			unitVect(destination.getY() - this.getPosition().getY()) + this.getPosition().getY());
		
		ChessPieceModel oneAheadPiece = getGame().getBoard().getPieceAt(oneAhead);
		
		if(oneAheadPiece == null){
			//if null no problems
			//move
			this.getGame().getBoard().removePieceFrom(oneAhead);
			this.getGame().getBoard().removePieceFrom(getPosition());
			this.getGame().getBoard().placePieceAt(this, oneAhead);
			if(!destination.equals(oneAhead)){
				//not done, do again
				moveToRec(destination, orig);
				
			}
		}
		else if(!oneAheadPiece.getOwner().equals(this.getOwner())){
			//the piece ahead is an enemy
			throw new IllegalMove(this, orig, destination);
		}
		else if(oneAheadPiece.getOwner().equals(this.getOwner())){
			//cant move through own piece, reset and throw error
			this.getGame().getBoard().removePieceFrom(getPosition());
			this.getGame().getBoard().placePieceAt(this, orig);
			throw new IllegalMove(this, orig, destination);
		}
	}
	public String toString(){
		return "pawn";
	}
	public void resetFirstMove() {
		firstMove = true;
	}
}